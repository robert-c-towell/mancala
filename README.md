# Mancala

I created this project in 2014 for a class in highschool. This is a web-app written in C++ 2011 that runs in the cig-bin on an apache server.

It runs in single-step increments. The state of the game is stored as parameters in the URL. The app considers every move available to the user and calculates the game state. Each of these predicted game states is represented in a link that the user can click that the user can click for every option available to them.

The application also features an AI to play against which will recursively build a tree of possible game-states, then chooses the option that will lead to the most optimal state. Difficulty is controlled by looking further into future game-states.

It's pretty hacky, but this was my first programming language and I didn't know how to learn at the time.